# Web Dev Template

## Dependencies
- ruby
- gem
- sass
- compass
- node.js with npm
- bower
- grunt-cli

## Usage

### Install and Setup

1.	Clone this repo.
2.	`cd` into it.
3.	`bower install` (front end dependencies)
4.	`npm install` (grunt.js project dependencies)

### Development

Run `grunt` from the project root.

It's default task will:

- watch and compile sass
- serve `/src` on http://0.0.0.0:8000/
- livereload css and html inside `/src`

Edit the following:

#### HTML

- index.html
- (add other files as required)

#### CSS

- /src/css/scss/_site.scss
- /src/css/scss/main.scss


#### JS

- /src/js/main.js
- /src/js/plugins.js
