module.exports = function(grunt) {

    // Project configuration
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        compass: {
            dev: {
                options: {
                    config: 'config.rb',
                    environment: 'development'
                }
            }
        },

        connect: {
            dev: {
                options: {
                    base: 'src',
                    livereload: true,
                    open: true
                }
            }
        },

        watch: {
            options: {
                interrupt: true,
                livereload: true
            },
            dev: {
                files: ['src/css/**/*.scss', 'src/*.html'],
                tasks: ['compass']
            }
        }
    });


    // Load plugins and tasks
    grunt.loadNpmTasks('grunt-contrib-compass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-connect');


    // Define custom tasks
    grunt.registerTask('default', ['compass:dev', 'connect:dev', 'watch:dev']);
};